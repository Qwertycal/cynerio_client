import './App.css';

import {Table} from './components/index'

function App() {
  return (
    <div className="App">
      {/* <Actions/> */}
      <Table/>
    </div>
  );
}

export default App;
