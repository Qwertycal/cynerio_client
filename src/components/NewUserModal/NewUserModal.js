import React, { useState } from 'react';
import axios from 'axios';
import './NewUserModal.scss';

function NewUserModal({getUsers}) {

    const [newUser, serNewUser] = useState('');

    const submitNewUser = () => {
        if (newUser.length > 0) {                        
            let bodyParameters = {
                name: newUser
            }

            axios.post(
                'http://' + window.location.hostname + ':3001/api/users/user',
                bodyParameters)
                .then(res => {
                    if (res.status === 201) {
                        serNewUser();
                        getUsers();
                        window.location.href = '#'
                    }
                })
                .catch(error => {
                    console.log('error on check user');
                });
        }

    }

    return (
        <>
            <button className="wrapper_modal">
                <a href="#demo-modal">Add User</a>
            </button>
            <div id="demo-modal" className="modal">
                <div className="modal_content">
                    <div className="new_user_modal_wraper">
                        <div className="box1">
                            <h3>Add a new user</h3>
                        </div>
                        <div className="box2">User</div>
                        <div className="box3">
                            <input type="text" className="new_user_input" onChange={(e) => serNewUser(e.target.value)} />
                        </div>
                        <div className="box4">
                            <button type="button" className='white_button'>
                                <a href="#" className="modal__close">Cancel</a>
                            </button>
                            <button type="button" className='blue_button' onClick={() => submitNewUser()}>Add User</button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default NewUserModal;