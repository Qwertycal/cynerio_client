import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './Table.scss';
import './../Actions/Actions.scss';
import { NewUserModal } from './../index'

function Table() {

    const [usersData, setUsersData] = useState([]);
    const [dateFilter, setDateFilter] = useState(false);
    const [nameFilter, setNameFilter] = useState(false);
    const [statusFilter, setStatusFilter] = useState(false);
    const [selectedRow, setSelectedRow] = useState(0);

    useEffect(() => {
        getUsers();
    }, []);

    //get all users
    const getUsers = () => {
        axios.get('http://' + window.location.hostname + ':3001/api/users')
            .then(res => {
                if (res.status === 200) {
                    setUsersData(res.data.result);
                }
            })
            .catch(error => {
                console.log('error on check user');
            });
    }

    // choosing specific row
    const selectRow = (userID) => {
        setSelectedRow(userID);
    }

    // sort users list by input
    const sortTableRowsByInput = (e) => {
        let tempUsersData = [...usersData];
        if (e.target.value !== '') {
            tempUsersData = tempUsersData.filter((item) => {
                return item.name.toLowerCase().search(
                    e.target.value) !== -1;
            });
            setUsersData(tempUsersData);
        } else {
            getUsers();
        }
    }

    //build rows for table
    const renderResultRows = (usersData) => {
        return usersData && usersData.map((user) => {
            return (
                <tr key={user.id} onClick={() => selectRow(user.id)} className={"tableRow " + (user.id === selectedRow ? "selected_row" : "")}>
                    {!nameFilter ? (<td data-title="Name">{user.name}</td>) : null}
                    {!statusFilter ? (<td data-title="Status">{user.status === 1 ? "Checked IN" : "Checked OUT"}</td>) : null}
                    {!dateFilter ? (<td data-title="Action Time">{user.action_time}</td>) : null}
                </tr>
            );
        })
    }

    const changeStatus = () => {
        if (selectedRow > 0) {
            let tempUsersData = [...usersData];
            tempUsersData[selectedRow - 1].status = tempUsersData[selectedRow - 1].status === 0 ? 1 : 0;
            setUsersData(tempUsersData);
        }
    }

    return (
        <div>
            <div className="wrapper">
                <div className="one">
                    <input type="text" name="fname" placeholder="Search Tables" className="search_input" onChange={(e) => sortTableRowsByInput(e)} />
                </div>
                <div className="two">
                    <button type="button" className='blue_button' onClick={() => changeStatus()}>Change Status</button>                    
                    <NewUserModal getUsers={getUsers}/>
                </div>
            </div>

            <div className="wrapper">
                <div className="one">
                    <label>
                        <input type="checkbox" defaultChecked={dateFilter} onChange={() => setDateFilter(!dateFilter)} />
                        Date
                    </label>

                    <label>
                        <input type="checkbox" defaultChecked={nameFilter} onChange={() => setNameFilter(!nameFilter)} />
                        Name
                    </label>

                    <label>
                        <input type="checkbox" defaultChecked={statusFilter} onChange={() => setStatusFilter(!statusFilter)} />
                        Status
                    </label>
                </div>
            </div>

            <table className="table">
                <thead>
                    <tr>
                        {!nameFilter ? (<th>Name</th>) : null}
                        {!statusFilter ? (<th>Status</th>) : null}
                        {!dateFilter ? (<th>Action Time</th>) : null}
                    </tr>
                </thead>
                <tbody>
                    {
                        usersData
                            ? renderResultRows(usersData)
                            : 'loader'}
                </tbody>
            </table>
        </div>
    )
}

export default Table;